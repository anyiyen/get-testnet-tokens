# -*- coding: utf-8 -*-

import pytest


def pytest_addoption(parser):
    # TODO: Need to support testing site
    parser.addoption("--address", action="store", default=[],
                     help="set wallet address")


@pytest.fixture(scope="function")
def address(request):
    parm = request.config.getoption("--address")
    return parm
