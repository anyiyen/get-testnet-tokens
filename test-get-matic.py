from time import sleep

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

import os
from ruamel.yaml import YAML


def test_get_tokens(address):
    # credential_folder = os.path.join('credential')
    #
    driver = webdriver.Chrome()
    #
    # yaml = YAML()
    # path = credential_folder + '/' + credential + '.yaml'
    # with open(path, encoding='UTF-8') as yaml_file:
    #     data = yaml.load(yaml_file)
    #
    # address = hex(data['address'])
    driver.get("https://faucet.polygon.technology/")
    sleep(2)

    wallet_address = driver.find_element(By.XPATH, "//input[@type='text']")
    wallet_address.send_keys(address)

    submit_btn = driver.find_elements(By.CLASS_NAME, "btn-primary")[0]
    submit_btn.click()
    sleep(2)

    confirm_btn = driver.find_elements(By.CLASS_NAME, "btn-primary")[1]
    confirm_btn.click()
    sleep(2)

    expected = "Request Submitted"
    text = driver.find_element(By.CLASS_NAME, "title").text
    assert expected == text

    driver.quit()
