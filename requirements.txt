ansi2html==1.4.0
appdirs==1.4.3
colorama==0.3.7
entrypoints==0.3
flake8==3.7.8
mccabe==0.6.1
packaging==16.8
Paver==1.2.4
py==1.4.32
pycodestyle==2.5.0
pyflakes==2.1.1
pyparsing==2.2.0
pytest==3.0.5
pytest-html==1.13.0
ruamel.yaml==0.16.12
selenium==3.141.0
six==1.11.0
urllib3==1.25.3
