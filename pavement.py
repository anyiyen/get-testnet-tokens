# -*- coding: utf-8 -*-

import os
from paver.easy import *


__author__ = 'nick.hung'


PROJ_DIR = os.getcwd()
PIP_REQ_PATH = os.path.join(PROJ_DIR, 'requirements.txt')
BUILD_DIR = os.path.join(PROJ_DIR, 'output')
VIRTUALENV_DIR = os.path.join(BUILD_DIR, 'venv')
REPORT_DIR = os.path.join(BUILD_DIR, 'report')
PYTHON_RUNTIME = 'python3'
PYTEST_PATH = os.path.join(VIRTUALENV_DIR, 'bin', 'pytest')


@task
def prepare_env():
    call_task('clean')
    call_task('create_virtualenv')


@task
def create_virtualenv():
    """
    Create virtual environment
    """
    _info('Create virtual env')
    os.makedirs(PROJ_DIR, exist_ok=True)
    sh(f"{_get_command_path(PYTHON_RUNTIME)} -m venv {VIRTUALENV_DIR}")

    _info('Start to install python libraries')
    pip_path = os.path.join(VIRTUALENV_DIR, 'bin', 'pip3')
    sh(f"{pip_path} install -r {PIP_REQ_PATH}")

    # pip_opts = "-U --no-index --default-timeout=60 --find-links %s" % PYPI_LINK
    # sh(options.pip_path + " install %s -r %s", pip_opts, options.pip_req_path)
    # sh('https_proxy={} {} install -r {}'.format(PROXY, pip_path, PIP_REQ_PATH))


@task
def execute_all_tests():
    """ Execute all test cases on different platform """
    call_task('prepare_env')
    call_task('execute_browser_tests')
    call_task('execute_android_tests')
    call_task('execute_ios_tests')


@task
@cmdopts([('browser=', 'b', 'Which browser you want to test')])
@cmdopts([('lang=', 'l', 'Which language you want to test')])
def execute_browser_tests(options):
    """
    Execute all test cases and generate junit and html report
    """
    _info("Execute all test cases and create test report")
    # Get the testing browser from cmd argument, default is Chrome

    browser = options.browser if hasattr(options, 'browser') else 'chrome'
    language = options.lang if hasattr(options, 'lang') else 'en-us'

    target = 'tests'
    cmd = generate_cmd(browser, target, language)
    # We ignore error here to avoid interrupt other command execution
    sh(cmd, False, True)

@task
@cmdopts([('browser=', 'b', 'Which browser you want to test')])
@cmdopts([('lang=', 'l', 'Which language you want to test')])
def execute_screenshot(options):
    """
    languages ['en-us', 'zh-tw', 'ja', 'zh-cn', 'de-de', 'es-mx', 'fr-fr', 'id', 'ko', 'pt-br', 'ru', 'th', 'tr', 'vi']
    """
    _info("Execute screenshot and generate html report")
    # Check the Jenkins and cmd parameter
    if os.environ.get('Browser') is None and hasattr(options, 'browser') is False:
        raise Exception("Please specify the browser")

    if os.environ.get('Language') is None and hasattr(options, 'lang') is False:
        raise Exception("Please specify the browser language")

    browsers = [options.browser] if os.environ.get('Browser') is None else os.environ.get('Browser').split(',')
    _info(f"Browser ====> {browsers}")

    languages = [options.lang] if os.environ.get('Language') is None else os.environ.get('Language').split(',')
    _info(f"Language ====> {languages}")

    for browser in browsers:
        for lang in languages:
            report_dir = os.path.join(BUILD_DIR, f"{browser}/{lang}")
            os.makedirs(report_dir, exist_ok=True)
            cmd = f"{PYTEST_PATH} --browser {browser} --lang {lang} -v -m screenshot --html=output/screenshot/{browser}/{lang}/report.html "
            _info(cmd)
            sh(cmd, False, True)


@task
def execute_android_tests():
    _info("Execute Android screenshot and create test report")
    browser = 'android'
    language = 'en-us'
    target = os.path.join('tests', 'test_screenshot.py')
    cmd = generate_cmd(browser, target, language)
    sh(cmd, False, True)


@task
def execute_ios_tests():
    _info("Execute iOS screenshot and create test report")
    browser = 'ios'
    language = 'en-us'
    target = os.path.join('tests', 'test_screenshot.py')
    cmd = generate_cmd(browser, target, language)
    sh(cmd, False, True)


def generate_cmd(browser, target, language):
    """ Generate the pytest execution command for different platform"""
    report_dir = os.path.join(BUILD_DIR, browser)
    os.makedirs(report_dir, exist_ok=True)
    xunit_report_path = os.path.join(report_dir, f"{browser}.xml")
    html_report_path = os.path.join(report_dir, f"{browser}.html")
    cmd = f"{PYTEST_PATH} --browser {browser} --lang {language} {target} " \
          f"-v --junitxml {xunit_report_path} --html={html_report_path}"
    return cmd


@task
def flake8():
    """
    Generate flake8 document
    """
    _info('generate flake8 document')
    flake8_path = os.path.join(VIRTUALENV_DIR, 'bin', 'flake8')
    flake8_report_path = os.path.join(BUILD_DIR, 'flake8-output.txt')

    sh(f"{flake8_path} --format=pylint --output-file={flake8_report_path}", ignore_error=True)


@task
def clean():
    """
    Clean all temporary files
    """
    _info('Start to clean output folder')
    sh(f"rm -rf {BUILD_DIR}")


def _get_command_path(command):
    """
    ex: which python3 => /usr/bin/python3
    """
    command_path = sh(f"which {command}", capture=True)
    return command_path.strip()


def _wrap_with(code):
    def inner(text, bold=False):
        c = code
        if bold:
            c = f"1;{c}"
        return f"\033[{c}m{text}\033[0m"
    return inner


green = _wrap_with('32')
magenta = _wrap_with('93')


def _info(message, *args):
    print(green(message % args))


def _warn(message, *args):
    print(magenta(message % args))
